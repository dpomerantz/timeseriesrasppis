// server/index.js
// based off of to start the tutorial found at https://eastus-1.azure.cloud2.influxdata.com/orgs/23bc459b29266295/new-user-setup/nodejs

const express = require("express");

const {InfluxDB, Point} = require('@influxdata/influxdb-client')

const token = process.env.INFLUXDB_TOKEN || "nX8S_XL5bcGCZHcvTbIsfy1f-E9hrxPeV07UofDFRdH8KGZSgydW5EmUcORL3HfS3EMK56PCGZSlj8SFUweCEQ==";
const url = 'https://eastus-1.azure.cloud2.influxdata.com'

const client = new InfluxDB({url, token})
let experimentName = "Sensor";
//addMeasurement(measurementName, "2", "temperature", 100.5);
//writeToDb(client);
//queryFromDb(client);
//average(client);

//let data = query(client, experimentName, "100", 10);
//let data2 = query(client, experimentName, null, 10);

//console.log("In the main");
//console.log(data);
//query(client, measurementName, 10);
const PORT = process.env.PORT || 3001;

const app = express();

app.get("/query", async (req,res) => {
    let sensorId = req.query.sensorId;
    let start = req.query.start;
    let end = req.query.end;

    console.log("start is " + start + " and end is " + end);
    res.json(await query(client, experimentName, sensorId, start, end));
})

app.get("/average", async (req, res) => {
    let start = req.query.start;
    let end = req.query.end;
    let sensorId = req.query.sensorId;
    let measurementName = req.query.measurement;
    if (measurementName !== "temperature" && measurementName !== "humidity") {
        return res.status(400).send("Must set measurement param to be either humidity or temperature");
    }
    res.json(await average(client, measurementName, experimentName, sensorId, start, end));
})

app.post("/measurement", async (req, res) => {
    let sensorId = req.query.sensorId;
    let temperature = req.query.temperature;
    let humidity = req.query.humidity;

    if (temperature == null && humidity == null) {
        return res.status(400).send("Must set either humidity or temperature parameter");
    }

    if (isNaN(parseFloat(temperature))) {
        return res.status(400).send("Temperature must be a floating point value");
    }

    if (isNaN(parseFloat(humidity))) {
        return res.status(400).send("Humidity must be a floating point value");
    }

    console.log(`Params: ${sensorId} , ${temperature}, ${humidity}`);
    res.json(await addMeasurement(experimentName, sensorId, temperature, humidity));
})

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});
/*
function writeToDb(client) {
    let org = `daniel.pomerantz@mail.mcgill.ca`
    let bucket = `bucket1`;
    
    let writeClient = client.getWriteApi(org, bucket, 'ns')
    
    for (let i = 0; i < 5; i++) {
      let point = new Point('measurement1')
        .tag('tagname1', 'tagvalue1')
        .intField('field1', i)
    
      void setTimeout(() => {
        writeClient.writePoint(point)
      }, i * 1000) // separate points by 1 second
    
      void setTimeout(() => {
        writeClient.flush()
      }, 5000)
    }

}
*/

async function addMeasurement(measurementCategoryName, sensorId, temperature, humidity) {
    let org = `daniel.pomerantz@mail.mcgill.ca`
    let bucket = `bucket1`;
    
    let writeClient = client.getWriteApi(org, bucket, 'ns')
    
      let point = new Point(measurementCategoryName)
        .tag('sensorId', sensorId);

        // assumes at least one of the fields is set!
        if (temperature) {
            point = point.floatField("temperature", temperature);
        }

        if (humidity) {
            point = point.floatField("humidity", humidity);
     //       point.fields({"temperature" : temperature, "humidity" : humidity })     
        }

        writeClient.writePoint(point);
        await writeClient.flush();
        return "Success";
}
/*
function queryFromDb(client, measurement) {
    let org = `daniel.pomerantz@mail.mcgill.ca`
    let bucket = `bucket1`;
    let queryClient = client.getQueryApi(org)
    let fluxQuery = `from(bucket: "bucket1")
     |> range(start: -10m)
     |> filter(fn: (r) => r._measurement == "${measurement}")`
    
    queryClient.queryRows(fluxQuery, {
      next: (row, tableMeta) => {
        const tableObject = tableMeta.toObject(row)
        console.log(tableObject)
      },
      error: (error) => {
        console.error('\nError', error)
      },
      complete: () => {
        console.log('\nSuccess')
      },
    })
}*/

async function query(client, measurement, sensorId, start, end) {
    let org = `daniel.pomerantz@mail.mcgill.ca`
    let bucket = `bucket1`;
    let queryClient = client.getQueryApi(org);
    console.log("New thing");
    console.log(`${sensorId ? "|> filter(fn : (r) => r.tag ==" + sensorId +")" : ""}`);
    let fluxQuery = `from(bucket: "bucket1")
     |> range(start: ${-start}m, stop: ${end? -end + "m" : "now()"})
     |> filter(fn: (r) => r._measurement == "${measurement}" 
     ${sensorId ? "and r.sensorId == \"" + sensorId +"\"" : ""}
      )
     `
    // 
    // |> filter(fn: (r) => r.tag == "sensorId")
   //   ${sensorId ? "|> filter(fn : (r) => r.sensorId ==" + sensorId +")" : ""}
    

     console.log(fluxQuery);

    let results = await queryClient.collectRows(fluxQuery, (row, tableMeta) => JSON.stringify(tableMeta.toObject(row)));
 //   console.log("here are the results");
 //  results.forEach(x => console.log(x));
 //console.log(results);
    return results;
        
}

async function average(client, measurementName, measurement, sensorId, start, end) {
    let org = `daniel.pomerantz@mail.mcgill.ca`
    let bucket = `bucket1`;
    let queryClient = client.getQueryApi(org)
    let fluxQuery = `from(bucket: "bucket1")
     |> range(start: ${-start}m, stop: ${end? -end + "m" : "now()"})
     |> filter(fn: (r) => r._measurement == "${measurement}"
     ${sensorId ? "and r.sensorId == \"" + sensorId +"\"" : ""}
     and r._field == \"${measurementName}\"
     )
     |> mean()`
    

     console.log(fluxQuery);

    let results = await queryClient.collectRows(fluxQuery, (row, tableMeta) => JSON.stringify(tableMeta.toObject(row)));
 //   console.log("here are the results");
 //  results.forEach(x => console.log(x));
 //console.log(results);
    return results;
        
}
/*
function average(client) {
    let org = `daniel.pomerantz@mail.mcgill.ca`
    let bucket = `bucket1`;

    queryClient = client.getQueryApi(org)
fluxQuery = `from(bucket: "bucket1")
 |> range(start: -10m)
 |> filter(fn: (r) => r._measurement == "measurement1")
 |> mean()`

queryClient.queryRows(fluxQuery, {
  next: (row, tableMeta) => {
    const tableObject = tableMeta.toObject(row)
    console.log("Calculating the average");
    console.log(tableObject)
    console.log("Done!");
  },
  error: (error) => {
    console.error('\nError', error)
  },
  complete: () => {
    console.log('\nSuccess')
  },
})
}*/